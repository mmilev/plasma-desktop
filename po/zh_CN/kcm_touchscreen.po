msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-09 02:14+0000\n"
"PO-Revision-Date: 2023-09-02 02:55\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf6-trunk/messages/plasma-desktop/kcm_touchscreen.pot\n"
"X-Crowdin-File-ID: 43245\n"

#: kcmtouchscreen.cpp:44
#, kde-format
msgid "Automatic"
msgstr "自动"

#: kcmtouchscreen.cpp:50
#, kde-format
msgctxt "model - (x,y widthxheight)"
msgid "%1 - (%2,%3 %4×%5)"
msgstr "%1 - (%2,%3 %4×%5)"

#: ui/main.qml:28
#, kde-format
msgid "No touchscreens found"
msgstr "未找到触摸屏"

#: ui/main.qml:44
#, kde-format
msgctxt "@label:listbox The device we are configuring"
msgid "Device:"
msgstr "设备："

#: ui/main.qml:57
#, kde-format
msgid "Enabled:"
msgstr "已启用："

#: ui/main.qml:65
#, kde-format
msgid "Target display:"
msgstr "目标显示器："
